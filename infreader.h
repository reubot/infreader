#ifndef infreader_H
#define infreader_H
#include <KJob>
#include <QtCore/QObject>
#include <KUrl>
#include <QtNetwork>
#include <stdint.h>
#include <QDateTime>
extern QHash<KUrl,QByteArray> urlCache;

class EpisodeInfo {

public:
        QString showTitle, episodeTitle,description, channel, filename;
        unsigned int seasonNum, episodeNum;
	QTime Duration;
	QDateTime TimeSlot;
//         EpisodeInfo(QByteArray);
// 	virtual EpisodeInfo();
	bool getYahoo();
	bool getTVDB();
	EpisodeInfo(QByteArray);
	EpisodeInfo(){};
};

class infreader : public QObject
{
Q_OBJECT
public:
    infreader(QUrl);
    virtual ~infreader();
private:
    QNetworkReply* qr;
    QNetworkAccessManager* qnam ;
    
signals:
    void infDataRead(EpisodeInfo);
private slots:
    void output();
    void process(KJob* job);
    void requestFinished(QNetworkReply *reply);
};

#endif // infreader_H
