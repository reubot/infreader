#include <QCoreApplication>
#include <KApplication>
#include <KCmdLineArgs>
#include <KAboutData>
#include "infreader.h"
#include "mainwindow.h"
#include <QStringList>


int main(int argc, char** argv)

{    KAboutData aboutData( "infreader", "infreader", ki18n("infReader"), "5", ki18n(   "<qt><p>infReader is a batch file renamer</p></qt>" ),
                          KAboutData::License_GPL, ki18n("infReader Build:") , ki18n("(c) 2001-2012, Dominik Seichter\n"),
                          "http://www.infreader.net", "domseichter@web.de" );
	KCmdLineArgs::init(argc,argv,&aboutData);
    KApplication app(true);//(argc, argv);


    
//    infreader foo(KUrl("ftp://guest@192.168.0.12:88/DataFiles/Kids' WB Saturday-6.mpg.inf"));
//   infreader foo(KUrl("ftp://guest@192.168.0.12:88DataFiles/Orphan Black - Nature Under Constraint.mpg.inf"));
   infreader foo(KUrl("ftp://guest@trf7170:88/DataFiles/Megastructures China's Smart Tower.mpg.inf"));
//    infreader foo(KUrl("ftp://guest@192.168.0.12:88/DataFiles/Aqua Teen Hunger Force - Super Computer.mpg.inf"));
  
    

  //   QObject::connect(&foo,SIGNAL(infDataRead(EpisodeInfo)), &bar, SLOT(addRow(EpisodeInfo)));
//      QStringList x;
//      x.append("dfdfd");
//      x.append("sdfsdds");
//      x.append("dfdfdf");
//      bar.AddRow(x);de
         MainWindow bar;
     
     bar.show();
    return app.exec();
}
