QMap(
     ("captions", QVariant(QString, "1") ) 
     ( "category" ,  QVariant
	     (QVariantList, 
		     (QVariant
			     (QVariantMap, QMap(
						       ("item", QVariant
								(QVariantList, 
									(QVariant
										(QVariantMap, QMap
											      (
												      ("0", QVariant(QString, "News and Documentary")) 
												      ( "seotext" ,  QVariant(QString, "news"))
												      )
											      )
										)
									)
								)
						       )
					   )
			     
			     )
		     )
	     )
     ( "co_v_short" , QVariant(QString, "ABC1")) 
     ( "colour" ,QVariant(QString, "1"))
     ( "description_1" ,  QVariant(QString, "Stephen Fry examines how gadgets entertain us, with the help of his friend Jeremy Clarkson.") ) 
     ( "end_date" ,  QVariant
	     (QVariantList, 
		     (QVariant
			     (QVariantMap, QMap
					   ( 
						   ("0", QVariant(QString, "1369963740") ) 
						   ( "timestamp" ,  QVariant(QString, "1369963740") ) 
						   ( "with_mm" ,  QVariant(QString, "11:29am") ) 
						   ( "without_mm" ,  QVariant(QString, "11:29")) 
						   )
					   ) 
			     )  
		     ) 
	     ) 
     ( "event_date" ,  QVariant
	     (QVariantList, 
		     (QVariant
			     (QVariantMap, QMap
					   (
						   ("0", QVariant(QString, "1369962180") ) 
						   ( "timestamp" ,  QVariant (QString, "1369962180") ) 
						   ( "with_mm" ,  QVariant(QString, "11:03am") ) 
						   ( "without_mm" ,  QVariant(QString, "11:03") )
					)
					   )
			     )
		     )
	     )
     ( "event_id" ,  QVariant(QString, "53122748") )
     ( "genre" ,  QVariant(QString, "Documentary") ) 
     ( "genre_id" ,  QVariant(QString, "4") ) 
     ( "program_id" ,  QVariant(QString, "4052856") ) 
     ( "rating" ,  QVariant(QString, "G") ) 
     ( "repeat" ,  QVariant(QString, "1") ) 
     ( "running_time" ,  QVariant(QString, "26") )
     ( "series_id" ,  QVariant(QString, "297295") )
     ( "show_type" ,  QVariant(QString, "Series") )
     ( "subtitle" ,  QVariant
	     (QVariantList, 
		     (QVariant
			     (QVariantMap, QMap
					   (
						   ("0", QVariant(QString, "Fun and Games")  )
						   ( "urlencoded" ,  QVariant(QString, "Fun+and+Games") ) 
						   
					)
					   )
			     )
		     )
	     )
     ( "title" ,  QVariant
	     (QVariantList, 
		     (QVariant
			     (QVariantMap, QMap
					   (
						   ("0", QVariant(QString, "Stephen Fry: Gadget Man") ) 
						   ( "urlencoded" ,  QVariant(QString, "Stephen+Fry%3A+Gadget+Man") )
						   )
					   )
			     )
		     )
	     )
     ( "venue_id" ,  QVariant(QString, "246952") )
     ( "year_released" ,  QVariant(QString, "2012") ) 
     )  
