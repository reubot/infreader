/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Reuben Peterkin <reuben_p@yahoo.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "dropqtablewidget.h"
#include <QDebug>
#include <QUrl>

DropQTableWidget::DropQTableWidget ( QWidget* parent ): QTableWidget(parent)
{

}

const QString pvr_host("192.168.0.12");

bool DropQTableWidget::dropMimeData(int row, int column, const QMimeData* data, Qt::DropAction action)
{
    qDebug() << "dropMimeData\t" << data->urls();
    return QTableWidget::dropMimeData(row, column, data, action);
}

Qt::DropActions DropQTableWidget::supportedDropActions() const
{
    return QTableWidget::supportedDropActions();
}

void DropQTableWidget::dropEvent(QDropEvent* event)
{
    qDebug() << "dropEvent\t" <<  event->mimeData()->urls();
    QUrl urli;
    foreach (urli, event->mimeData()->urls())
    {
        qDebug() << urli << urli.password();
        if (urli.path().endsWith(".inf"))// and (urli.host() ==pvr_host or urli.host().)
        {
            qDebug() << urli.userInfo() << urli.host();
            urli.setPort(88);
            infreader *foobar = new infreader(urli);
            QObject::connect(foobar,SIGNAL(infDataRead(EpisodeInfo)), this, SLOT(addRow(EpisodeInfo)));
            
        }
            
    }
//    QTableWidget::dropEvent(event);
}

void DropQTableWidget::dragEnterEvent(QDragEnterEvent* event)
{
    QAbstractItemView::dragEnterEvent(event);
//    if(event->mimeData()->hasFormat("text/plain"))
        event->acceptProposedAction();
    
}

void DropQTableWidget::dragMoveEvent(QDragMoveEvent* event)
{
    QAbstractItemView::dragMoveEvent(event);
    event->accept();
}

void DropQTableWidget::dragLeaveEvent(QDragLeaveEvent* event)
{
    QAbstractItemView::dragLeaveEvent(event);
}
void DropQTableWidget::addRow(EpisodeInfo qsl)
{
    qDebug() << "addRow";
    setSortingEnabled(false);
    int rowc = rowCount();
    insertRow(rowc);
    
     setItem(rowc, 0, new QTableWidgetItem(qsl.filename));
     setItem(rowc, 1, new QTableWidgetItem(qsl.showTitle));
     setItem(rowc, 2, new QTableWidgetItem(qsl.episodeTitle));
     setItem(rowc, 3, new QTableWidgetItem(qsl.description));     
    setRowHeight(rowc,50);
     
  
 setSortingEnabled(true);
}
