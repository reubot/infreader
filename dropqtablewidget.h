/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Reuben Peterkin <reuben_p@yahoo.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef DROPQTABLEWIDGET_H
#define DROPQTABLEWIDGET_H

#include <QTableWidget>
#include <QDropEvent>
#include "infreader.h"
class DropQTableWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit DropQTableWidget(QWidget *parent = 0);
//    DropQTableWidget(int rows, int columns, QWidget *parent = 0);

protected:
    virtual bool dropMimeData(int row, int column, const QMimeData* data, Qt::DropAction action);
    virtual Qt::DropActions supportedDropActions() const;
    virtual void dropEvent(QDropEvent* event);
    virtual void dragEnterEvent(QDragEnterEvent* event);
    virtual void dragMoveEvent(QDragMoveEvent* event);
    virtual void dragLeaveEvent(QDragLeaveEvent* event);
public slots:
    void addRow(EpisodeInfo);
};

#endif // DROPQTABLEWIDGET_H
