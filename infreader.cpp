#include "infreader.h"

#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QFtp>
//#include <KJob>
#include <KIO/Job>
#include <KIO/StoredTransferJob>
#include <iostream>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <stdint.h>
#include <KIO/NetAccess>

#include <QDomDocument>
#include <KDebug>
#include <KFile>
#include <qjson/parser.h>
QHash<KUrl,QByteArray> urlCache;

EpisodeInfo  loadtvdbXML(EpisodeInfo epInfo);

EpisodeInfo* readPostFromDomNode(QDomNode node,QString showName, QString desc)
{
	EpisodeInfo* showing = new EpisodeInfo;
    QString xmlShow, xmlEpisode, xmlDesc;
    while ( !node.isNull() ) {
        QDomElement elm = node.toElement();
             if ( elm.tagName() == "title")
           showing->showTitle = elm.text();
	else if ( elm.tagName() == "sub-title" )
            showing->episodeTitle = elm.text();
        else if ( elm.tagName() == "desc" )
            showing->description = elm.text();
        else if ( elm.tagName() == "episode-num" and elm.attribute("system") == "xmltv_ns" )
	{
            showing->seasonNum =    elm.text().split('.').first().toInt() + 1;
	    showing->episodeNum=    elm.text().split('.').at(1)  .toInt() + 1;
	    qDebug() << showing->seasonNum << showing->episodeNum  << elm.text() ;
	}
/*        else if ( elm.tagName() == "summary" and post->content.length() == 0)
            post->content = elm.text();
	else if ( elm.tagName() == "id" )
            post->postId = elm.text();

        else if ( elm.tagName() == "link" and elm.attribute("rel") == "alternate" )
            post->link = elm.attribute("href");
	else if ( elm.tagName() == "in_reply_to_status_id" )
            post->replyToPostId = elm.text();
        else if ( elm.tagName() == "in_reply_to_user_id" )
            post->replyToUserId = elm.text();
        else if ( elm.tagName() == "in_reply_to_screen_name" )
            post->replyToUserName = elm.text();
        else if ( elm.tagName() == "twitter:source" )
            post->source = elm.text();
        else if ( elm.tagName() == "favorited" )
            post->isFavorited = ( elm.text() == "true" ) ? true : false;
        else if ( elm.tagName() == "statusnet:conversation_id" )
            post->conversationId = elm.text();
        else if ( elm.tagName() == "author" ) {
            QDomNode node3 = node.firstChild();
            while ( !node3.isNull() ) {
                QDomElement elm3 = node3.toElement();
                if ( elm3.tagName() == "screen_name" ) {
                    post->author.userName = elm3.text();
//                 } else if ( elm3.tagName() == "profile_image_url" ) {
//                     post->author.profileImageUrl = elm3.text();
                } else if ( elm3.tagName() == "id" ) {
                    post->author.userId = elm3.text();
                } else if ( elm3.tagName() == "name" ) {
                    post->author.realName = elm3.text();
		    post->author.userName = elm3.text();
                } else if ( elm3.tagName() == QString ( "description" ) ) {
                    post->author.description = elm3.text();
                } else if ( elm3.tagName() == "uri" ) {
                    post->author.homePageUrl = elm3.text();
                } else if ( elm3.tagName() == "protected" ) {
                    post->author.isProtected = elm3.text().contains("true") ? true : false;
                }
                node3 = node3.nextSibling();;
            }
        } else if ( elm.tagName() == "retweeted_status" )
            repeatedPost = readPostFromDomNode( theAccount, elm.firstChild(), new Choqok::Post);
*/
        node = node.nextSibling();
    }
    
    QString descmatchnew = desc;
    QString descmatchold = showing->description;
    descmatchnew.remove('"').remove('\'');
    descmatchold.remove('"').remove('\'');
    if (showing->showTitle == showName and descmatchold == descmatchnew /*showing->description.remove('"') == desc*/)
    {
	    std::cout << "Episode\t"<< showing->episodeTitle.toStdString()<< std::endl;
	    qDebug() << "description" << showing->description << "desc" << desc;
	    return showing;
    }
    return NULL;
   
}


EpisodeInfo* parseXML(QByteArray buffer,QString showName, QString desc)

{

    QDomDocument document;
    document.setContent ( buffer );
    QDomElement root = document.documentElement();	
    
    if ( root.tagName() != "tv" ) {
        //         QString err = i18n( "Data returned from server is corrupted." );
        qDebug() << "there's no tv tag in XML\t the XML is: \n" << buffer;
        return NULL;
    }
    
    QDomNode node = root.firstChild();
    while ( !node.isNull() ) {
	    QDomElement nodeElm = node.toElement();
		if ( nodeElm.tagName() == "programme" ) {
			QDomNode node2 = nodeElm.firstChild();			    
			EpisodeInfo* ep = readPostFromDomNode(node2,showName,desc);
			if (ep != NULL)
				return ep;	    
		}
			else if ( nodeElm.tagName() == "channel" ) {
				qDebug() << "channel" << nodeElm.attribute("id") ;
			}
			else  {
				qDebug() << "there's no programme  tag in XML, Error!!\t"
				<<"Tag is: "<<nodeElm.tagName();
				return NULL;
			}

    

        node = node.nextSibling();
    }
	
	
	return NULL;
	
}


bool EpisodeInfo::getYahoo()


{
	bool finalresult = false;
	EpisodeInfo yahooDetails;
	QString ts;ts.setNum(this->TimeSlot.toMSecsSinceEpoch()/1000);
	QString dur;dur.setNum(this->TimeSlot.addSecs(QTime(0, 0, 0).secsTo(this->Duration)).toMSecsSinceEpoch()/1000);
// 	std::cout << "Start time\t" << ts << std::endl;
	KUrl yahooURL("https://au.tv.yahoo.com/tv-guide/data/75/0/" + ts + "/" +  dur + "/");
	qDebug() << yahooURL.url();
	QByteArray yahooJSON;

	if (urlCache.contains(yahooURL))
		yahooJSON = urlCache.value(yahooURL);
	else{
	
		KIO::Job *xmljob = KIO::get(yahooURL);
		QMap<QString, QString> metaDataXML;
		if ( KIO::NetAccess::synchronousRun( xmljob, 0, &yahooJSON, new KUrl, &metaDataXML ) ) {
			QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		//	qDebug()<<"Response header = "<< responseHeaders << "length =" << yahooJSON.length();
			
		}	
			
		if (yahooJSON.isEmpty() || yahooJSON.isNull() || yahooJSON.length() <= 1)
			return false; //return empty
		
			urlCache.insert(yahooURL,yahooJSON);
	}
	// create a Parser instance
	QJson::Parser parser;

	bool ok;

	// json is a QString containing the data to convert
	QVariantMap result = parser.parse (yahooJSON, &ok).toMap();
	
qDebug() << "yahooOK" << ok;
	QVariantList itemList = result.value("tv").toList().first().toMap().value("item").toList();
	QVariantList::iterator i;
#define TIME_DIFF 500 /* in seconds */
	for (i = itemList.begin(); i != itemList.end(); ++i)
	{
		QVariantMap itemMap = (*i).toMap();
		
		QString yahooTitle	= itemMap.value("title").toList().first().toMap().value("0").toString() ;
		QDateTime yahooStart	= QDateTime::fromMSecsSinceEpoch(itemMap.value("event_date").toList().first().toMap().value("timestamp").toString().toLong()*1000);
		QDateTime yahooEnd	= QDateTime::fromMSecsSinceEpoch(itemMap.value("end_date").toList().first().toMap().value("timestamp").toString().toLong()*1000);
		QString yahooDesc	= itemMap.value("description_1").toString() ;
		if (!yahooTitle.compare(this->showTitle,Qt::CaseInsensitive) 
			&& ((this->TimeSlot >= yahooStart.addSecs(-TIME_DIFF) && this->TimeSlot <= yahooStart.addSecs(TIME_DIFF)) 
			|| yahooDesc.compare(this->episodeTitle,Qt::CaseInsensitive) == 0 || yahooDesc.contains(this->episodeTitle,Qt::CaseInsensitive)
		))
			
		{
		//	qDebug() << this->episodeTitle << this->episodeTitle.compare("After a Beastie Boys Heads concert at Madison Cube Garden, Bender gets hooked on power surges. Guest Starring: The Beastie Boys and Dan Castellaneta",Qt::CaseInsensitive);;
//			qDebug() << "yahoo" << this->showTitle << itemMap.values();
			qDebug() << "ycompare" <<  yahooDesc.compare(this->episodeTitle,Qt::CaseInsensitive) << yahooDesc.contains("After a Beastie Boys Heads concert at Madison Cube Garden, Bender gets hooked on power surges. Guest Starring: The Beastie Boys and Dan Castellaneta",Qt::CaseInsensitive);
			yahooDetails.showTitle	= yahooTitle; 
			if (itemMap.contains("subtitle"))
			{
				 yahooDetails.episodeTitle = this->episodeTitle	= itemMap.value("subtitle").toList().first().toMap().value("0").toString();
				finalresult = true;
			}
			//else
				
			this->description = yahooDesc;
			qDebug() << "yahooEp" << yahooDetails.showTitle <<"\t"  << yahooDetails.episodeTitle;// << itemMap.value("subtitle").toList().first().toMap().value("0").toString();
			qDebug() << "yahooStart+range" << yahooStart <<  yahooStart.addSecs(-TIME_DIFF) << yahooStart.addSecs(TIME_DIFF);
			qDebug() << "yahooEnd\t\t" << yahooEnd;
			qDebug() << "yahooDesc\t" << yahooDesc;
	//		finalresult = true;
		}
	}

//qDebug() << itemList.first().toMap();
	return finalresult;
}



EpisodeInfo parseYahoo(EpisodeInfo epgInfo)


{
	EpisodeInfo yahooDetails;
	QString ts;ts.setNum(epgInfo.TimeSlot.toMSecsSinceEpoch()/1000);
	QString dur;dur.setNum(epgInfo.TimeSlot.addSecs(QTime(0, 0, 0).secsTo(epgInfo.Duration)).toMSecsSinceEpoch()/1000);
// 	std::cout << "Start time\t" << ts << std::endl;
	KUrl yahooURL("https://au.tv.yahoo.com/tv-guide/data/75/0/" + ts + "/" +  dur + "/");
	qDebug() << yahooURL.url();
	QByteArray yahooJSON;
	
	KIO::Job *xmljob = KIO::get(yahooURL);
	QMap<QString, QString> metaDataXML;
	if ( KIO::NetAccess::synchronousRun( xmljob, 0, &yahooJSON, new KUrl, &metaDataXML ) ) {
		QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		qDebug()<<"Response header = "<< responseHeaders << "length =" << yahooJSON.length();
		
	}	
		
if (yahooJSON.isEmpty() || yahooJSON.isNull() || yahooJSON.length() <= 1)
	return yahooDetails; //return empty
	// create a Parser instance
	QJson::Parser parser;

	bool ok;

	// json is a QString containing the data to convert
	QVariantMap result = parser.parse (yahooJSON, &ok).toMap();
	
qDebug() << "yahooOK" << ok;
	QVariantList itemList = result.value("tv").toList().first().toMap().value("item").toList();
	QVariantList::iterator i;
#define TIME_DIFF 500 /* in seconds */
	for (i = itemList.begin(); i != itemList.end(); ++i)
	{
		QVariantMap itemMap = (*i).toMap();
		
		QString yahooTitle	= itemMap.value("title").toList().first().toMap().value("0").toString() ;
		QDateTime yahooStart	= QDateTime::fromMSecsSinceEpoch(itemMap.value("event_date").toList().first().toMap().value("timestamp").toString().toLong()*1000);
		QDateTime yahooEnd	= QDateTime::fromMSecsSinceEpoch(itemMap.value("end_date").toList().first().toMap().value("timestamp").toString().toLong()*1000);
		QString yahooDesc	= itemMap.value("description_1").toString() ;
		if (!yahooTitle.compare(epgInfo.showTitle,Qt::CaseInsensitive) 
			&& ((epgInfo.TimeSlot >= yahooStart.addSecs(-TIME_DIFF) && epgInfo.TimeSlot <= yahooStart.addSecs(TIME_DIFF)) 
			|| yahooDesc.compare(epgInfo.episodeTitle,Qt::CaseInsensitive) == 0 || yahooDesc.contains(epgInfo.episodeTitle,Qt::CaseInsensitive)
		))
			
		{
		//	qDebug() << epgInfo.episodeTitle << epgInfo.episodeTitle.compare("After a Beastie Boys Heads concert at Madison Cube Garden, Bender gets hooked on power surges. Guest Starring: The Beastie Boys and Dan Castellaneta",Qt::CaseInsensitive);;
			qDebug() << "yahoo" << epgInfo.showTitle << yahooTitle;
			qDebug() << "ycompare" <<  yahooDesc.compare(epgInfo.episodeTitle,Qt::CaseInsensitive) << yahooDesc.contains("After a Beastie Boys Heads concert at Madison Cube Garden, Bender gets hooked on power surges. Guest Starring: The Beastie Boys and Dan Castellaneta",Qt::CaseInsensitive);
			yahooDetails.showTitle	= yahooTitle; 
			if (itemMap.contains("subtitle"))
			yahooDetails.episodeTitle	= itemMap.value("subtitle").toList().first().toMap().value("0").toString();
			
			qDebug() << "yahoo" << yahooDetails.showTitle <<"\t"  << yahooDetails.episodeTitle;
			qDebug() << "yahooStart+range" << yahooStart <<  yahooStart.addSecs(-TIME_DIFF) << yahooStart.addSecs(TIME_DIFF);
			qDebug() << "yahooEnd\t\t" << yahooEnd;
			qDebug() << "yahooDuration\t" << yahooDesc;
		}
	}

//qDebug() << itemList.first().toMap();
	return yahooDetails;
}


EpisodeInfo::EpisodeInfo(QByteArray ba)
{
	

	for (uint i = 0x2A; ba.at(i) != 0;i++)
		this->channel.append(ba.at(i));	

	
	uint nameLen = (uint)ba.at(0x55);	
	for (uint i = 0x57; i < 0x57+nameLen;i++)
		this->showTitle.append(ba.at(i));	
	
	for (uint i = 0x57+nameLen; ba.at(i) != 0;i++)
		this->episodeTitle.append(ba.at(i));	
	
	for (uint i = 0x170; ba.at(i) != 0;i++)
		this->description.append(ba.at(i));		
	
	QDateTime timeslot(QDate(1858,11,17),QTime(ba.at(9),ba.at(8)));
	int dur_minutes = ba.at(0xC) % 60;
	int dur_hours = ba.at(0xC) / 60;
	QTime duration(dur_hours,dur_minutes,ba.at(0xE));
        uint16_t days= ba.at(0xB)<<8   |(ba.at(0xA)&0xFF);
	timeslot = timeslot.addDays(days);	
	this->TimeSlot = timeslot;
	this->Duration	= duration;
	qDebug() << "parseInf channel\t" << this->channel;	
	qDebug() << "parseInf title\t" << this->showTitle;	
	qDebug() << "parseInf episode\t" << this->episodeTitle;	
	qDebug() << "parseInf desc\t" << this->description;
	qDebug() << "parseInf timeslot\t" << this->TimeSlot;
	qDebug() << "parseInf duration\t" << this->Duration;
}
EpisodeInfo parseArray(QByteArray ba)

{
	EpisodeInfo infDetails(ba);
// 	parseInf(ba);
// 	std::cout << "ba.size()\t" << ba.length() << std::endl;
// 	std::cout << "ba.at(0)\t"<< (uint)ba.at(0)<< std::endl;
// 	uint nameLen = (uint)ba.at(0x55);
// 	
// 	QString channel;
// 	for (uint i = 0x2A; ba.at(i) != 0;i++)
// 		channel.append(ba.at(i));
// 	std::cout << "Channel\t"<< channel.toStdString()<< std::endl;
//         
// 
// 	QString showName;
// 	for (uint i = 0x57; i < 0x57+nameLen;i++)
// 		showName.append(ba.at(i));
// 
// 	std::cout << "Show Name\t"<< showName.toStdString()<< std::endl;
// 	QString episodeName;
// 	for (uint i = 0x57+nameLen; ba.at(i) != 0;i++)
// 		episodeName.append(ba.at(i));
// 	std::cout << "Episode\t"<< episodeName.toStdString()<< std::endl;
// 
// 	QDateTime timeslot(QDate(1858,11,17),QTime(ba.at(9),ba.at(8)));
// // 	std::cout << day.toString().toStdString()<< std::endl;
// 	
// 	QTime duration(0,ba.at(0xC),ba.at(0xE));
//         uint16_t days= ba.at(0xB)<<8   |(ba.at(0xA)&0xFF);
// //        uint8_t duractio= ba.at(0xB)<<8   |(ba.at(0xA)&0xFF);
// // 	std::cout << std::dec << ba.at(0xC) << std::endl;
// 	timeslot = timeslot.addDays(days);
// 	std::cout << "Start time\t" << timeslot.toString().toStdString()<< "\t" << timeslot.toMSecsSinceEpoch()/1000<< std::endl;
//         std::cout << "Duration\t" << duration.toString().toStdString()<< "\t" << QTime(0, 0, 0).secsTo(duration) << std::endl;
// //	std::cout << "Duraction\t" << std::dec << ba.at(0xC)<<8 << "mins " << std::dec << ba.at(0xE) <<"secs" << std::endl;
// 	QString desc;
// 	for (uint i = 0x170; ba.at(i) != 0;i++)
// 		desc.append(ba.at(i));
// 	std::cout << "desc\t"<< desc.toStdString()<< std::endl;
// 
//         infDetails.channel 	= channel;
//         infDetails.showTitle	= showName;
//         infDetails.episodeTitle = episodeName;
//         infDetails.description	= desc;
// 	infDetails.TimeSlot	= timeslot;
// 	infDetails.Duration	= duration;
//         
	QString channel = infDetails.channel;
	QString showName = infDetails.showTitle;
	QString episodeName = infDetails.episodeTitle;
	std::cout << "desc\t"<< infDetails.description.length();
	
	//parseYahoo(infDetails);
//	loadtvdbXML(parseYahoo(infDetails));
		infDetails.getYahoo();
		infDetails.getTVDB();
	if (channel == "ELEVEN")
		
	{
//		KUrl xmltvurl("http://www.oztivo.net/xmltv/"+channel+"_"+timeslot.date().toString("yyyy-MM-dd")+".xml.gz"); 
 		KUrl xmltvurl("file:///home/reuben/.shepherd/output.xmltv");
//		KUrl xmltvurl("file:///tmp/asd.txt");
// 		qDebug()<< timeslot.date().year();
//qDebug() <<"http://www.oztivo.net/xmltv/"+channel+"_"+timeslot.date().toString("yyyy-MM-dd")+".xml.gz";
// 		KIO::Job *xmljob = KIO::get(KUrl("http://www.oztivo.net/xmltv/"+channel+"_2012-08-15.xml.gz") );
 		KIO::Job *xmljob;
		QByteArray xmltv;
		if (xmltvurl.isLocalFile())
		{
			qDebug() << xmltvurl.toLocalFile();
			QFile xmlfile(xmltvurl.toLocalFile());
			xmlfile.open(QIODevice::ReadOnly | QIODevice::Text);
			qDebug() << xmlfile.size();
			xmltv = xmlfile.readAll();
// 			qDebug() << xmlfile.readLine();
// 			qDebug() << xmlfile.readLine();
// 			qDebug() << xmlfile.readLine();
//			qDebug() << xmltv;
		}
		else
		{
		xmljob = KIO::get( xmltvurl);
//		KIO::Job *xmljob = KIO::get(KUrl("file:///home/reuben/.shepherd/output.xmltv") );
		QMap<QString, QString> metaDataXML;
		metaDataXML.insert( "PropagateHttpHeader", "true" );
		
		if ( KIO::NetAccess::synchronousRun( xmljob, 0, &xmltv, new KUrl, &metaDataXML ) ) {
			QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
			qDebug()<<"Response header = "<< responseHeaders;
		}}
		EpisodeInfo* newepisode = parseXML(xmltv,showName,episodeName);
		if (newepisode != NULL)
		{
		qDebug() << "newepisode->episodeTitle" << newepisode->episodeTitle;			
			episodeName = newepisode->episodeTitle;
			QString epString;
			if (newepisode->seasonNum != NULL)
			{
				QString sStr = QString::number(newepisode->seasonNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("s"+sStr);
			}
			if (newepisode->episodeNum != NULL)
			{
				QString sStr = QString::number(newepisode->episodeNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("e"+sStr);
			}
			if (!epString.isEmpty())
				qDebug() << epString;
		}
	}
	return infDetails;
}

QString parseGetSeries(QByteArray buffer,QString showName)

{

    QDomDocument document;
    document.setContent ( buffer );
    QDomElement root = document.documentElement();	
    
    if ( root.tagName() != "Data" ) {
        //         QString err = i18n( "Data returned from server is corrupted." );
        qDebug() << "there's no data tag in XML\t the XML is: \n" << buffer;
        return NULL;
    }
    
    QDomNode node = root.firstChild();
    while ( !node.isNull() ) {
	    QDomElement nodeElm = node.toElement();
		if ( nodeElm.tagName() == "Series" ) {
			QDomNode node2 = nodeElm.firstChild();		
			QString seriesid ,SeriesName;
			 while ( !node2.isNull() ) {
				 
        QDomElement elm = node2.toElement();
             if ( elm.tagName() == "seriesid")
            seriesid = elm.text();
	else if ( elm.tagName() == "SeriesName" )
             SeriesName = elm.text();
//         else if ( elm.tagName() == "desc" )
//             showing->description = elm.text();
//         else if ( elm.tagName() == "episode-num" and elm.attribute("system") == "xmltv_ns" )
// 	{
//             showing->seasonNum =    elm.text().split('.').first().toInt() + 1;
// 	    showing->episodeNum=    elm.text().split('.').at(1)  .toInt() + 1;
// 	    qDebug() << showing->seasonNum << showing->episodeNum  << elm.text() ;
// 	}
		
	
				  node2 = node2.nextSibling();
			}
	qDebug() << seriesid << SeriesName;	
	if (showName.compare(SeriesName,Qt::CaseInsensitive)==0 ||
		(showName.toLower() == "aqua teen hunger force" && SeriesName.toLower() == "aqua tv show show"))
	    return seriesid ;
// 			if (ep != NULL)
// 				return ep;	    
// 		}
// 			else if ( nodeElm.tagName() == "channel" ) {
// 				qDebug() << "channel" << nodeElm.attribute("id") ;
// 			}
// 			else  {
// 				qDebug() << "there's no programme  tag in XML, Error!!\t"
// 				<<"Tag is: "<<nodeElm.tagName();
// 				return NULL;
// 			}

    
}
        node = node.nextSibling();
		
    }
	
	
	return NULL;
	
}

bool parseSeries(QByteArray buffer,EpisodeInfo *epInfo)

{

    QDomDocument document;
    document.setContent ( buffer );
    QDomElement root = document.documentElement();	
    
    if ( root.tagName() != "Data" ) {
        //         QString err = i18n( "Data returned from server is corrupted." );
        qDebug() << "there's no data tag in XML\t the XML is: \n" << buffer;
        return false;
    }
    else
	    qDebug() << "parsing Series Record" ;//<< buffer;
    
    QDomNode node = root.firstChild();
    while ( !node.isNull() ) {
	    QDomElement nodeElm = node.toElement();
		if ( nodeElm.tagName() == "Episode" ) {
			QDomNode node2 = nodeElm.firstChild();		
			QString EpisodeID ,EpisodeName;
			unsigned int seasonNum, episodeNum;
			 while ( !node2.isNull() ) {
				 
				QDomElement elm = node2.toElement();
				if ( elm.tagName() == "id")
				EpisodeID = elm.text();
				else if ( elm.tagName() == "EpisodeName" )
				EpisodeName = elm.text();
				else if ( elm.tagName() == "Combined_episodenumber" )
				episodeNum = (unsigned int) elm.text().toFloat();
				else if ( elm.tagName() == "Combined_season" )
				seasonNum = elm.text().toInt();			
//         else if ( elm.tagName() == "desc" )
//             showing->description = elm.text();
//         else if ( elm.tagName() == "episode-num" and elm.attribute("system") == "xmltv_ns" )
// 	{
//             showing->seasonNum =    elm.text().split('.').first().toInt() + 1;
// 	    showing->episodeNum=    elm.text().split('.').at(1)  .toInt() + 1;
// 	    qDebug() << showing->seasonNum << showing->episodeNum  << elm.text() ;
// 	}
		
	
				  node2 = node2.nextSibling();
			}
			//qDebug() << EpisodeID << EpisodeName << seasonNum << episodeNum;
			if (epInfo->episodeTitle.compare(EpisodeName,Qt::CaseInsensitive)==0)
			{
				qDebug() << EpisodeID << EpisodeName << seasonNum << episodeNum;
				epInfo->episodeNum = episodeNum;
				epInfo->seasonNum = seasonNum;
				return true;
			//return EpisodeID ;
			
			}
// 			if (ep != NULL)
// 				return ep;	    
// 		}
// 			else if ( nodeElm.tagName() == "channel" ) {
// 				qDebug() << "channel" << nodeElm.attribute("id") ;
// 			}
// 			else  {
// 				qDebug() << "there's no programme  tag in XML, Error!!\t"
// 				<<"Tag is: "<<nodeElm.tagName();
// 				return NULL;
// 			}

    
}
        node = node.nextSibling();
		
    }
	
	
	return false;
	
}
bool  EpisodeInfo::getTVDB()
{
	EpisodeInfo yahooDetails;
//	QString ts;ts.setNum(epInfo.TimeSlot.toMSecsSinceEpoch()/1000);
//	QString dur;dur.setNum(epInfo.TimeSlot.addSecs(QTime(0, 0, 0).secsTo(epInfo.Duration)).toMSecsSinceEpoch()/1000);
// 	std::cout << "Start time\t" << ts << std::endl;
	KUrl tvdbURL("http://thetvdb.com/api/GetSeries.php?seriesname="+this->showTitle);
	
	
	
	qDebug() << tvdbURL.url();
	QByteArray tvdbXML;
	
	if (urlCache.contains(tvdbURL))
		tvdbXML = urlCache.value(tvdbURL);
	else{
	
	KIO::Job *xmljob = KIO::get(tvdbURL);
	QMap<QString, QString> metaDataXML;
	if ( KIO::NetAccess::synchronousRun( xmljob, 0, &tvdbXML, new KUrl, &metaDataXML ) ) {
		QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		qDebug()<<"Response header = "<< responseHeaders << "length =" << tvdbXML.length();
		
	}	
		
	if (tvdbXML.isEmpty() || tvdbXML.isNull() || tvdbXML.length() <= 1)
		return false; //return empty
	
	
	qDebug() <<tvdbXML ;
	

	
	
 	urlCache.insert(tvdbURL,tvdbXML);
 	}
		QString seriesid = parseGetSeries(tvdbXML, this->showTitle);	
		
	if (seriesid.isEmpty() || seriesid.isNull() || seriesid.length() <= 1)
		return false; //return empty		
		//http://thetvdb.com/api/4CC11E56E9735C66/series/73871/all/en.xml
		tvdbURL = KUrl("http://thetvdb.com/api/4CC11E56E9735C66/series/"+seriesid+"/all/en.xml");
		
 	if (urlCache.contains(tvdbURL))
 		tvdbXML = urlCache.value(tvdbURL);
 	else{
	
	 KIO::Job *xmljob = KIO::get(tvdbURL);
	QMap<QString, QString> metaDataXML;
	if ( KIO::NetAccess::synchronousRun( xmljob, 0, &tvdbXML, new KUrl, &metaDataXML ) ) {
		QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		qDebug()<<"Response header = "<< responseHeaders << "length =" << tvdbXML.length();
		
	}	
		
	if (tvdbXML.isEmpty() || tvdbXML.isNull() || tvdbXML.length() <= 1)
		return false; //return empty
	
	
	//qDebug() <<tvdbXML ;
	

	
	//
 	urlCache.insert(tvdbURL,tvdbXML);
 	}		
//		
		return parseSeries(tvdbXML, this);	
		
		
		
/*		
		EpisodeInfo* newepisode = parseXML(xmltv,showName,episodeName);
		if (newepisode != NULL)
		{
		qDebug() << "newepisode->episodeTitle" << newepisode->episodeTitle;			
			episodeName = newepisode->episodeTitle;
			titles.insert("episodeName",episodeName);
			QString epString;
			if (newepisode->seasonNum != NULL)
			{
				QString sStr = QString::number(newepisode->seasonNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("s"+sStr);
			}
			if (newepisode->episodeNum != NULL)
			{
				QString sStr = QString::number(newepisode->episodeNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("e"+sStr);
			}
			if (!epString.isEmpty())
				titles.insert("epString",epString);
			
		}
		return titles;
		*/

return false;
	}

	
 
EpisodeInfo  loadtvdbXML(EpisodeInfo epInfo)
{
	EpisodeInfo yahooDetails;
//	QString ts;ts.setNum(epInfo.TimeSlot.toMSecsSinceEpoch()/1000);
//	QString dur;dur.setNum(epInfo.TimeSlot.addSecs(QTime(0, 0, 0).secsTo(epInfo.Duration)).toMSecsSinceEpoch()/1000);
// 	std::cout << "Start time\t" << ts << std::endl;
	KUrl tvdbURL("http://thetvdb.com/api/GetSeries.php?seriesname="+epInfo.showTitle);
	
	
	
	qDebug() << tvdbURL.url();
	QByteArray tvdbXML;
	
// 	if (urlCache.contains(tvdbURL))
// 		tvdbXML = urlCache.value(tvdbURL);
// 	else{
	
	KIO::Job *xmljob = KIO::get(tvdbURL);
	QMap<QString, QString> metaDataXML;
	if ( KIO::NetAccess::synchronousRun( xmljob, 0, &tvdbXML, new KUrl, &metaDataXML ) ) {
		QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		qDebug()<<"Response header = "<< responseHeaders << "length =" << tvdbXML.length();
		
	}	
		
	if (tvdbXML.isEmpty() || tvdbXML.isNull() || tvdbXML.length() <= 1)
		return yahooDetails; //return empty
	
	
	qDebug() <<tvdbXML ;
	

	
	
 	urlCache.insert(tvdbURL,tvdbXML);
// 	}
		QString seriesid = parseGetSeries(tvdbXML, epInfo.showTitle);	
		
	if (seriesid.isEmpty() || seriesid.isNull() || seriesid.length() <= 1)
		return yahooDetails; //return empty		
		//http://thetvdb.com/api/4CC11E56E9735C66/series/73871/all/en.xml
		tvdbURL = KUrl("http://thetvdb.com/api/4CC11E56E9735C66/series/"+seriesid+"/all/en.xml");
		
// 	if (urlCache.contains(tvdbURL))
// 		tvdbXML = urlCache.value(tvdbURL);
// 	else{
	
	 xmljob = KIO::get(tvdbURL);
//	QMap<QString, QString> metaDataXML;
	if ( KIO::NetAccess::synchronousRun( xmljob, 0, &tvdbXML, new KUrl, &metaDataXML ) ) {
		QString responseHeaders = metaDataXML[ "HTTP-Headers" ];
		qDebug()<<"Response header = "<< responseHeaders << "length =" << tvdbXML.length();
		
	}	
		
	if (tvdbXML.isEmpty() || tvdbXML.isNull() || tvdbXML.length() <= 1)
		return yahooDetails; //return empty
	
	
	//qDebug() <<tvdbXML ;
	

	
	//
// 	urlCache.insert(tvdbURL,tvdbXML);
// 	}		
//		
		parseSeries(tvdbXML, &epInfo);	
		return epInfo;
		
		
/*		
		EpisodeInfo* newepisode = parseXML(xmltv,showName,episodeName);
		if (newepisode != NULL)
		{
		qDebug() << "newepisode->episodeTitle" << newepisode->episodeTitle;			
			episodeName = newepisode->episodeTitle;
			titles.insert("episodeName",episodeName);
			QString epString;
			if (newepisode->seasonNum != NULL)
			{
				QString sStr = QString::number(newepisode->seasonNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("s"+sStr);
			}
			if (newepisode->episodeNum != NULL)
			{
				QString sStr = QString::number(newepisode->episodeNum);
				if (sStr.length() ==1)
					sStr.prepend('0');
				epString.append("e"+sStr);
			}
			if (!epString.isEmpty())
				titles.insert("epString",epString);
			
		}
		return titles;
		*/

return epInfo;
	}

	

infreader::infreader(QUrl location)
{
    
    bool synchronous = false;
    if (synchronous){
//   KIO::StoredTransferJob* infftp = KIO::storedGet(KUrl("ftp://guest:0000@192.168.0.12/DataFiles/The Flintstones-3.mpg.inf"));
//   connect(infftp,SIGNAL(finished(KJob*)),SLOT(process(KJob*)));
/*
	QFile* inf = new QFile("/tmp/The Flintstones.mpg.inf");
	inf->open(QIODevice::ReadOnly);
	std::cout << "inf->size()\t" << inf->size() << std::endl;
	QByteArray ba = inf->readAll();
//	parseArray(ba);
	*/
        KIO::Job *job = KIO::get(location );
        QMap<QString, QString> metaData;
        metaData.insert( "PropagateHttpHeader", "true" );
        QByteArray bana;
        if ( KIO::NetAccess::synchronousRun( job, 0, &bana, new KUrl, &metaData ) ) {
        QString responseHeaders = metaData[ "HTTP-Headers" ];
        qDebug()<<"Response header = "<< responseHeaders;
        }
 parseArray(bana);


	
	exit(0);	
}
else
    
{qnam = new QNetworkAccessManager();
       
    
     QNetworkRequest qnr = QNetworkRequest(location);
     qr = qnam->get(qnr);

     connect(qnam, SIGNAL(finished(QNetworkReply*)), SLOT(requestFinished(QNetworkReply*)));
}
}

infreader::~infreader()
{}

void infreader::output()
{
    std::cout << "Hello World!" << std::endl;
}

void infreader::process(KJob* job)
{
	QByteArray ba = qobject_cast<KIO::StoredTransferJob*>(job)->data();
	parseArray(ba);
	exit(0);
}

void infreader::requestFinished(QNetworkReply *reply)
{
        reply->ignoreSslErrors();
        std::cout << "requestFinished" << std::endl;    
        QByteArray bana = reply->readAll();
	if (bana.size() <= 0)
	{std::cout << "Empty File or File Not Found";
		exit(1);
	}
        EpisodeInfo infInfo = parseArray(bana);
        infInfo.filename =  reply->url().path();
        emit infDataRead(infInfo);
//        exit(0);
        
        
//         qDebug() <<reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() << reply->url();
//         
//         
//         
//         QList<QNetworkCookie>  cookies = qnam->cookieJar()->cookiesForUrl(reply->url());
//         qDebug() << "COOKIES for" << reply->url().host() << cookies;    
//         if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 302)
//         {
//                 QUrl loginurl = reply->header(QNetworkRequest::LocationHeader).toUrl();
//                 qDebug() <<loginurl;
//                 QNetworkRequest xqnr = QNetworkRequest(loginurl);
// //              xqnr.setAttribute(QNetworkRequest::CookieSaveControlAttribute,true);
//         QVariant var;
//         var.setValue(cookies);
//         xqnr.setHeader(QNetworkRequest::CookieHeader,var);              
//                 qnam->get(xqnr);
//                 return;
//                 
//                 
//         }
}

#include "infreader.moc"
